﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FontSymbolData.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace FontBuilder.PhotoshopLogic
{
    using System;
    using System.Linq;
    using System.Windows;

    /// <summary>
    /// Class definition for FontSymbolData.cs.
    /// </summary>
    public class FontSymbolData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FontSymbolData"/> class.
        /// </summary>
        /// <param name="c"></param>
        public FontSymbolData(char c)
        {
            Character = c;
        }

        /// <summary>
        /// Gets or sets the character or font symbol.
        /// </summary>
        public char Character { get; set; }

        /// <summary>
        /// <para>
        /// Gets or sets the bounding rectangle of the individual character when drawn
        /// in photoshop. This is the size of the element drawn all alone
        /// inside of a text layer that is fixed near, (but not at) the 
        /// top left corner. The position of the top-left corner of the
        /// text layer is Globals.BUFFER_SPACE_BETWEEN_CHARACTERS away from
        /// both the top and the left edge of the canvas.
        /// </para>
        /// <para>
        /// This bounding rectangle includes all styles, drop shadows, italics,
        /// outer glows, etc. that the user may have configured via 
        /// layer styles/blending options.
        /// </para>
        /// <para>
        /// Before creating the font bitmap, each character is drawn 
        /// individually, by itself, inside the same text layer, to calculate
        /// the bounds of each character. The "draw line" of the photoshop
        /// text layer does not ever change, so this bounding rectangle is
        /// calculated uniformly for each character.
        /// </para>
        /// <para>
        /// Once the photoshop bounds of each character have been determined,
        /// and we have determined the tallest (or rather, the highest-positioned,
        /// closest-to the top, character) we can calculate the VerticalOffset
        /// of every other character or symbol.
        /// </para>
        /// </summary>
        public Rect PhotoshopBounds { get; set; }

        /// <summary>
        /// <para>
        /// Gets or sets the vertical offset for this character.
        /// </para>
        /// <para>
        /// Every character in photoshop is drawn along an invisible
        /// "draw line". The draw line is the imaginary line where
        /// the font is drawn on (an "A" will be completely on top of the draw
        /// line, whereas a "g" will be drawn partly below the draw line). 
        /// </para>
        /// <para>
        /// However, in XNA, we always draw all graphics and fonts from
        /// the top-left corner. This means that we must align each font
        /// character in relation to tallest (highest, not tallest) character
        /// in the symbol set. The first pixel of the tallest (highest) character
        /// is essentially the top of our font rectangle when drawing in xna.
        /// </para>
        /// <para>
        /// This vertical offset is the distance from the top of the tallest 
        /// (highest, not tallest) font character in the symbol set, to the top 
        /// of this character.
        /// </para>
        /// </summary>
        public int VerticalOffset { get; set; }

        /// <summary>
        /// Gets the width of this character when drawn in photoshop. 
        /// This will include all style elements such as drop shadows, italics, 
        /// outer glows, etc.
        /// </summary>
        public int Width
        {
            get
            {
                return (int)PhotoshopBounds.Width;
            }
        }

        /// <summary>
        /// Gets the height of this character when drawn in photoshop. This will 
        /// include all style elements such as drop shadows, italics, 
        /// outer glows, etc.
        /// </summary>
        public int Height
        {
            get
            {
                return (int)PhotoshopBounds.Height;
            }
        }
    }
}
