﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SpritefontFileBuilder.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace FontBuilder.PhotoshopLogic
{
    using System;
    using System.IO;
    using System.Linq;
    using Sector12Common.Helpers;

    /// <summary>
    /// Class definition for SpritefontFileBuilder.cs.
    /// </summary>
    public static class SpritefontFileBuilder
    {
        /// <summary>
        /// Creates a .spritefont file. This is needed if we create any symbols
        /// aside from basic english. XNA will use this file to understand what
        /// symbols and character ranges we are using. Again, this is not needed
        /// for basic english symbols. Check out this page for more info:
        /// http://theinstructionlimit.com/common-kanji-character-ranges-for-xna-spritefont-rendering
        /// </summary>
        /// <param name="d"></param>
        /// <param name="fontName"></param>
        /// <param name="overwrite"></param>
        public static void BuildSpritefontFile(DirectoryInfo d, string fontName, bool overwrite)
        {
            // Create a new .spritefont file. Name it FontName.spritefont. The same
            // .spritefont file will be used for all font sizes.
            string path = d.FullName + "\\" + fontName + ".spritefont";

            // Add a number to the end of the file name if it already exists,
            // and we don't want to overwrite.
            if (!overwrite)
                path = FileSystemHelper.GetAlternateFileNameToAvoidOverwrite(path);

            // Build the .spritefont xml
            string xml = BuildSpritefontXml();

            // Save the file. The file contents are overwritten automatically if
            // the file already exists.
            File.WriteAllText(path, xml);
        }

        /// <summary>
        /// Builds the .spritefont xml for our selected language.
        /// </summary>
        /// <returns>The xml for our .spritefont file.</returns>
        private static string BuildSpritefontXml()
        {
            // Not implemented yet
            return "";
        }
    }
}
