﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SymbolLoader.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace FontBuilder.PhotoshopLogic
{
    using System;
    using System.Linq;
    using Sector12Common.ResourceLoaders;

    /// <summary>
    /// The purpose of this class is to load the full list of symbols we want
    /// to build from a text file included in this project. The list of symbols
    /// will be different depending on what language the user has selected to
    /// build.
    /// </summary>
    public static class SymbolLoader
    {
        /// <summary>
        /// Build only the most common list of english characters and symbols. 
        /// This is the character set that XNA uses and expects by default.
        /// If you use another language/character set than this, you must also
        /// provide XNA with a custom .spritefont file.
        /// </summary>
        /// <returns>The most common English symbols.</returns>
        public static string LoadCommonEnglishSymbols()
        {
            Uri uri = new Uri("pack://application:,,,/Content/LanguageSymbols/basic_english.txt");
            return ReadFromEmbeddedTextFile(uri);
        }

        /// <summary>
        /// Build extended ASCII symbols. This is the common list of english
        /// characters plus french accents, etc. Pull this list from a text
        /// file that I've created manually, and included in the project.
        /// </summary>
        /// <returns>The set of extended ASCII symbols.</returns>
        public static string LoadExtendedASCIISymbols()
        {
            Uri uri = new Uri("pack://application:,,,/Content/LanguageSymbols/extended_ascii.txt");
            return ReadFromEmbeddedTextFile(uri);
        }

        /// <summary>
        /// Loads Korean Hangul symbols. Pull this list from a text file that 
        /// I've created manually, and included in the project.
        /// </summary>
        /// <returns>The set of Korean hangul characters.</returns>
        public static string LoadKoreanHangul()
        {
            Uri uri = new Uri("pack://application:,,,/Content/LanguageSymbols/korean_hangul.txt");
            return ReadFromEmbeddedTextFile(uri);
        }

        /// <summary>
        /// Reads all symbols from an embedded text file. 
        /// </summary>
        /// <param name="packUri"></param>
        /// <returns>The symbol string.</returns>
        private static string ReadFromEmbeddedTextFile(Uri packUri)
        {
            // Load the contents of our embedded text file into a string
            string fileContents = WpfResourceLoader.LoadResource<string>(packUri, ResourceLoaderContentType.Text);

            // For readability and human line/symbol counting purposes, the 
            // embedded file probably has one symbol per line, instead of one 
            // huge long string with no spaces. Remove line breaks if they exist.
            fileContents = fileContents.Replace(Environment.NewLine, "");

            return fileContents;
        }
    }
}
