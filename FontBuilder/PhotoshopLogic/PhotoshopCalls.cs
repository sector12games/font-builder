﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PhotoshopCalls.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace FontBuilder.PhotoshopLogic
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Windows;
    using Photoshop;
    using Sector12Common.PhotoshopHelpers;
    using Sector12Common.ResourceLoaders;

    /// <summary>
    /// Class definition for PhotoshopCalls.cs.
    /// </summary>
    public static class PhotoshopCalls
    {
        private static bool _isPhotoshopVisible;
        private static Photoshop.Application _ps;
        private static Document _templateDoc;
        private static Document _currentDoc;
        private static LayerSet _characterLayerSet;

        private static ArtLayer _fontLayer;
        private static ArtLayer _fontCopyLayer;
        private static ArtLayer _magentaBackgroundLayer;
        private static ArtLayer _blackReadabilityBackgroundLayer;

        private static SolidColor _white;
        private static SolidColor _black;
        private static SolidColor _magenta;

        /// <summary>
        /// Gets or sets the symbol data.
        /// </summary>
        public static List<FontSymbolData> SymbolData { get; set; }

        /// <summary>
        /// Gets or sets the height of the symbol set.
        /// </summary>
        public static int SymbolSetHeight { get; set; }

        /// <summary>
        /// <para>
        /// Gets a value indicating whether or not photoshop is visible.
        /// </para>
        /// <para>
        /// This value is necessary because of photoshop's stupidity. Even checking
        /// to see if photoshop is visible (_ps.Visible) will throw an exception 
        /// if photoshop is busy doing something else (like selecting/editing text).
        /// Thus, we'll keep this variable here, and only access the real value
        /// from within photoshop when we knows it's safe to.
        /// </para>
        /// </summary>
        public static bool IsPhotoshopVisible
        {
            get
            {
                return _isPhotoshopVisible;
            }

            private set
            {
                _isPhotoshopVisible = value;
            }
        }

        /// <summary>
        /// Initializes photoshop for the build.
        /// </summary>
        public static void InitializePhotoshop()
        {
            // Start up photoshop, if it is not already open.
            _ps = new Photoshop.Application();

            // Set photoshop variables
            _ps.Preferences.RulerUnits = PsUnits.psPixels;
            _ps.Preferences.TypeUnits = PsTypeUnits.psTypePixels;
            _ps.DisplayDialogs = PsDialogModes.psDisplayNoDialogs;

            // Create various colors that will be needed for building the font
            _white = new SolidColor();
            _white.RGB.Red = 255;
            _white.RGB.Green = 255;
            _white.RGB.Blue = 255;

            _black = new SolidColor();
            _black.RGB.Red = 0;
            _black.RGB.Green = 0;
            _black.RGB.Blue = 0;

            _magenta = new SolidColor();
            _magenta.RGB.Red = 255;
            _magenta.RGB.Green = 0;
            _magenta.RGB.Blue = 255;

            // Initialize our symbol data list
            SymbolData = new List<FontSymbolData>();
        }

        /// <summary>
        /// <para>
        /// Creates a new photoshop document that we will use as a base for creating
        /// multiple font sizes (or just one size if we are not building multiple sizes 
        /// in a batch). 
        /// </para>
        /// <para>
        /// All of the characters and symbols that we want to build will be added to 
        /// this template and will be presented to the user for further modification. 
        /// The user can add styles, drop shadows, glows, etc. via photoshop layer 
        /// styles/blending options. These styles will then be used for every font 
        /// size that we want to build.
        /// </para>
        /// <para>
        /// Each font size that we build will be created in it's own photoshop doucment,
        /// so that the user can go into photoshop after it has completed, and view
        /// or modify each file individually if he cares to. Each new document will
        /// be duplicated from this template via the artLayer.Duplicate() method.
        /// </para>
        /// </summary>
        /// <param name="symbolsToBuild"></param>
        public static void CreateTemplatePhotoshopDocument(string symbolsToBuild)
        {
            // Shorter name for convenience
            int buffer = Globals.BufferSpaceBetweenCharacters;

            // Set the background color to magenta
            _ps.BackgroundColor = _magenta;

            // Load a default photoshop file in photoshop, with a single text layer
            // that contains all of the requested symbols. Keep in mind that we
            // cannot set the initial fill to "psBackgroundColor". It would be nice
            // if we could, as this creates a locked "background" layer, that always
            // keeps its color filled, even if we expand the canvas. However, we cannot
            // cut pieces out of a locked "background" layer in order to create 
            // transparent blocks around each of our characters. In order to do this,
            // we must set the background fill to transparent (which means a standard
            // layer will be created instead of a locked background). It's the same 
            // as if we right-click and select "layer from background...". In addition,
            // we cannot create a layer mask on a locked background either. It will
            // automatically convert the background to a standard layer. In other words,
            // we're stuck having to manually fill new areas of the canvas with the
            // magenta color any time we expand the canvas.
            // *** NOTE: 7/15/2013
            // Some time later, I decided to include a black background as well, to
            // make it easier to read the white text as it was being built. I attempted
            // to set this background to PsDocumentFill.psBackgroundColor as well, but
            // there were strange issues when expanding the canvas. In the end, I 
            // decided to stay with standard layers (psTransparent).
            _templateDoc = _ps.Documents.Add(
                Globals.StartingCanvasWidth,
                Globals.StartingCanvasHeight,
                InitialFill: PsDocumentFill.psTransparent);
            
            // ***NOTE***
            // When executed from c#, photoshop collections (like ArtLayers) 
            // are indexed starting at 1. However, when executed from javascript,
            // photoshop collections are indexed from 0. This is a very 
            // interesting issue, which I will write quite a bit about here, as 
            // it took me a while to track down physical evidence of my suspicions. 
            //
            // Here's some things we need to know:
            // - Photoshop uses a custom built scripting engine called "ExtendScript"
            //    - ExtendScript can understand VBScript, Javascript, or AppleScript
            //    - Photoshop collections are handled a bit differently in each of the
            //      three languages. They're not even called collections in AppleScript,
            //      they are called elements.
            //    - Javascript and AppleScript collections start at an index of 0.
            //      VBScript collections start at an index of 1.
            //      See the following pdf document, page 11 "Photoshop Object Model" for
            //      more details. Pay special attention to the note at the bottom.
            //      http://wwwimages.adobe.com/www.adobe.com/content/dam/Adobe/en/devnet/photoshop/pdfs/photoshop_cs5_scripting_guide.pdf
            //
            // - My theory is that when executing interop code directly from c#, 
            //   the ExtendScript engine handles all calls as VBScript. I could
            //   not find a way to verify that the c# assemblies are just a wrapper
            //   around VBScript, but I did find this page, that verifies collections
            //   are 1-based when running interop code from .NET (in his case Visual Basic):
            //   http://lonerobot.net/?page_id=1079
            //
            // Grab a reference to the background layer. It will be the very
            // last layer in the layer array. Remember, in c#, photoshop collections
            // start at an index of 1. Be careful, because executing the same code
            // from javascript will break. Photoshop collections in javascript are 0-based.
            _blackReadabilityBackgroundLayer = _templateDoc.ArtLayers[_templateDoc.ArtLayers.Count];
            _blackReadabilityBackgroundLayer.Name = "black layer";

            // Add our magenta layer
            _magentaBackgroundLayer = _templateDoc.ArtLayers.Add();
            _magentaBackgroundLayer.Name = "magenta layer";

            // Add a text layer named "font", which will contain all of 
            // the symbols we want to build
            _fontLayer = _templateDoc.ArtLayers.Add();
            _fontLayer.Kind = PsLayerKind.psTextLayer;
            _fontLayer.Name = "font";

            // Set the font style to be the font type to a default value. The user can
            // change the font when we pause in just a few seconds.
            _fontLayer.TextItem.Font = Globals.DefaultFont;

            // Add all of the symbols that we want to build into the text layer.
            // PrepareFontContents() makes the original font string that the user
            // can tamper with and add styles to more readable (adds line breaks,
            // etc).
            _fontLayer.TextItem.Contents = PrepareFontConents(symbolsToBuild);
            _fontLayer.TextItem.Color = _white;
            _fontLayer.TextItem.Size = Globals.DefaultTemplateFontSize;

            // Reposition the font layer to be a specific distance from the
            // top left corner. By default, when programattically adding a 
            // new text layer, it is already positioned somewhat close to the 
            // top left corner, but I haven't found any documentation stating
            // EXACTLY the position that it will always be in. I think it's around
            // (25, 25), where the y value is actually the "draw line" position,
            // not the top-left corner. The draw line is the imaginary line where
            // the font is drawn on (an "A" will be completely on top of the draw
            // line, whereas a "g" will be drawn partly below the draw line). This
            // means that the actual top-left corner position will change
            // depending on the size of the characters within the string.
            // So to make sure we're always starting in the same place, lets
            // reposition the canvas to where the top-left corner (not the draw line)
            // is always in the same location. Remember, this can only be done via the
            // translate() function, which is relative, not absolute positioning.
            // http://stackoverflow.com/questions/12064015/photoshop-scripting-move-an-image-to-position-x-y
            PhotoshopDrawingHelpers.MoveLayerToAbsolutePosition(_fontLayer, buffer, buffer);

            // Our text string is most likely longer than the starting canvas
            // size. We want to expand the canvas to ensure we display all
            // of the text elements we just added.
            _templateDoc.RevealAll();

            // We revealed everything, so we know the canvas is now displaying
            // everything. However, RevealAll() only makes the canvas JUST
            // wide enough to show all elements. We want to create a bit of a
            // buffer of space on the right and bottom edges. In other words,
            // expand the canvas slightly more.
            _templateDoc.ResizeCanvas(
                _templateDoc.Width + buffer,
                _templateDoc.Height + buffer,
                PsAnchorPosition.psTopLeft);

            // Now that we've added all of the symbol set characters and expanded
            // the canvas, select all and apply a fill to both our magenta and
            // black background layers.
            _templateDoc.Selection.SelectAll();
            _templateDoc.ActiveLayer = _magentaBackgroundLayer;
            _templateDoc.Selection.Fill(_magenta);
            _templateDoc.ActiveLayer = _blackReadabilityBackgroundLayer;
            _templateDoc.Selection.Fill(_black);

            // Finally, create a new "layer set". Later, we will create
            // a new temporary layer for each character that we want to
            // build. As we build more and more characters, we don't want
            // to have tons of individual character layers. The layer set
            // will allow us to merge all characters into a single layer.
            _characterLayerSet = _templateDoc.LayerSets.Add();
            _characterLayerSet.Name = "characters";
        }

        /// <summary>
        /// Creates a secondary font layer, which is an exact copy of
        /// the original font layer, that can be used for drawing, determining 
        /// individual character sizes and bounds, etc. without harming the
        /// original layer.
        /// </summary>
        public static void CreateFontCopyLayer()
        {
            _fontCopyLayer = _fontLayer.Duplicate();
            _fontCopyLayer.Name = "font copy";
        }

        /// <summary>
        /// <para>
        /// Creates a new document that we will use to build a font. This document
        /// will be duplicated from the template document we created earlier. The
        /// only difference will be the font size.
        /// </para>
        /// <para>
        /// A new document will be created for each font size that we want to build
        /// if we are building a batch.
        /// </para>
        /// </summary>
        /// <param name="fontSize"></param>
        public static void CreateFontBuilderPhotoshopDocument(int fontSize)
        {
            // Duplicate our template document
            // ***NOTE***. There appears to be a bug in photoshop. If I call 
            // _templateDoc.Duplicate(), it won't necessarily duplicate 
            // _templateDoc, like you would expect. Instead, it duplicates
            // whichever document is currently active in photoshop. So, make
            // sure to make the template document active before duplicating.
            _ps.ActiveDocument = _templateDoc;
            _currentDoc = _templateDoc.Duplicate();

            // Update our local variables to point at the new document
            _fontLayer = _currentDoc.ArtLayers["font"];
            _fontCopyLayer = _currentDoc.ArtLayers["font copy"];

            // Get a reference to the new background layers as well. Remember
            // that this is starting at index 1, not 0. Read my comment further
            // up in the document regarding _backgroundLayer for more info.
            _blackReadabilityBackgroundLayer = _currentDoc.ArtLayers[_currentDoc.ArtLayers.Count];
            _magentaBackgroundLayer = _currentDoc.ArtLayers[_currentDoc.ArtLayers.Count - 1];

            // Also get a reference to the new layer set
            _characterLayerSet = _currentDoc.LayerSets["characters"];

            // Update the font sizes in the text layers
            _fontLayer.TextItem.Size = fontSize;
            _fontCopyLayer.TextItem.Size = fontSize;

            // Clear all symbol data. We will have to rebuild it since our
            // font size has changed.
            SymbolData.Clear();
        }

        /// <summary>
        /// Builds size and bounds information for a single character or symbol.
        /// Vertical offset cannot be calculated at this point - we have to wait
        /// until all symbols dimensions have been determined.
        /// </summary>
        /// <param name="c"></param>
        public static void BuildSymbolData(char c)
        {
            // Draw the rectangle onto a temporary layer in photoshop and determine
            // the bounding rectangle. The calculateStringBoundsRect() method will use the
            // same text layer to draw characters every time it is called. This means
            // that the "draw line" will be the same for every character. However, 
            // the bounding rect (top y value, left x value) will be different, 
            // depending on the height and width of each individual character.
            // By using the bounds of every character drawn on the same "draw line",
            // we can determine the tallest character (not really tallest, but highest 
            // positioned), and also calculate the vertical offsets of all other
            // characters shorter than this tallest character. 
            FontSymbolData data = new FontSymbolData(c);
            data.PhotoshopBounds = PhotoshopFontHelpers.CalculateStringBoundsRect(
                _fontCopyLayer, 
                c.ToString());

            // Add the data to our master list
            SymbolData.Add(data);
        }

        /// <summary>
        /// <para>
        /// Calculates and sets the vertical offsets for each symbol. This 
        /// method can only be ran after all symbols have determined their
        /// bounds.
        /// </para>
        /// <para>
        /// The vertical offset of a symbol is the distance from the very 
        /// top of the symbol to the very top of the symbol that sticks up 
        /// highest.
        /// </para>
        /// </summary>
        public static void CalculateVerticalOffsets()
        {
            // First, we need to determine the tallest (highest-positioned,
            // not actually tallest) character or symbol in our symbol set.
            FontSymbolData highestSymbol = null;
            foreach (FontSymbolData data in SymbolData)
            {
                if (highestSymbol == null || data.PhotoshopBounds.Top < highestSymbol.PhotoshopBounds.Top)
                    highestSymbol = data;
            }

            // Now that we've determined the character that is closest to the
            // top of the canvas, we can calculate the vertical offsets for
            // all other elements. 
            foreach (FontSymbolData data in SymbolData)
            {
                // Determine the distance from the top of this symbol, to
                // the top of the highest-positioned symbol. This will only
                // be a few pixels.
                data.VerticalOffset = (int)data.PhotoshopBounds.Top - (int)highestSymbol.PhotoshopBounds.Top;
            }
        }

        /// <summary>
        /// <para>
        /// Calculates the height of our entire symbol set. Imagine every character
        /// in our set drawn in one big line. This method determines how tall that
        /// string is.
        /// </para>
        /// <para>
        /// This is not necessarily the height of the tallest symbol. Some symbols
        /// can stick up very high off of the draw line ($) and some can go down
        /// very low (g). It's the distance from the top of the highest symbol to
        /// the bottom of the lowest symbol that we are looking for.
        /// </para>
        /// </summary>
        public static void CalculateSymbolSetHeight()
        {
            // Attempt to find the highest and the lowest possible draw points,
            // considering every character in our symbol set.
            int highestPoint = (int)SymbolData.First().PhotoshopBounds.Top;
            int lowestPoint = (int)SymbolData.First().PhotoshopBounds.Bottom;

            foreach (FontSymbolData data in SymbolData)
            {
                if (data.PhotoshopBounds.Top < highestPoint)
                    highestPoint = (int)data.PhotoshopBounds.Top;

                if (data.PhotoshopBounds.Bottom > lowestPoint)
                    lowestPoint = (int)data.PhotoshopBounds.Bottom;
            }

            // The total symbol set height is the distance between the highest
            // and the lowest draw points possible for all characters
            SymbolSetHeight = lowestPoint - highestPoint;
        }

        /// <summary>
        /// Draws a single symbol at the draw position specified. It will
        /// also delete a portion of the background around the specified
        /// symbol, so that it is contained within a transparent box.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="drawPosition"></param>
        public static void DrawSymbol(FontSymbolData data, Point drawPosition)
        {
            // Deselect everything before we start
            _currentDoc.Selection.Deselect();

            // Put the single character into a new temporary text layer.
            // This text layer will eventually be merged into other character
            // layers so that all fully built character symbols will be 
            // contained within a single layer. Remember to duplicate our
            // original font layer so that that the temporary text layer 
            // is an exact copy of the primary font layer, and has all of 
            // the same drop shadows, styles, etc.
            ArtLayer tempLayer = _fontLayer.Duplicate();
            tempLayer.TextItem.Contents = data.Character.ToString();

            // Add the temp layer to the "characters" layer set
            tempLayer.Move(_characterLayerSet, PsElementPlacement.psPlaceAtEnd);

            // Now we need to move the layer to the desired paste location. 
            // Remember that moving in photoshop needs to be done via relative
            // coordinates, using the translate() method. Remember to add in the
            // vertical offset/buffer as well.
            PhotoshopDrawingHelpers.MoveLayerToAbsolutePosition(
                tempLayer, 
                (int)drawPosition.X, 
                (int)drawPosition.Y + data.VerticalOffset);

            // Expand the canvas if the symbol we just added was partially
            // drawn outside of the canvas.
            ExpandCanvasIfNecessary();

            // Now we need to select a square around the character we just 
            // pasted in. We need to select the square, and clear it, so 
            // that the magenta background does not appear where the character 
            // is located. This is how xna will determine what portions of the
            // canvas are characters and which portions of the canvas are 
            // transparent (magenta). The width of the selection box should
            // be the same as the width of the character. The height however,
            // should be the height of the entire symbol set. The selection
            // bounds is an array of arrays. 
            // ***Do not create jagged arrays or multidimensional arrays using
            // standard c# syntax. Photoshop is very picky about the format of
            // the arrays you pass in. The following will NOT work:
            //   - int[][] selectionBounds
            //   - int[,] selectionBounds
            // You must create a single dimensional object array, and populate
            // each index with another single dimensional array.
            int x = (int)drawPosition.X;
            int y = (int)drawPosition.Y;

            object[] selectionBounds = new object[4];
            selectionBounds[0] = new object[] { x, y }; // top-left corner
            selectionBounds[1] = new object[] { x + data.Width, y }; // top-right corner
            selectionBounds[2] = new object[] { x + data.Width, y + SymbolSetHeight }; // bottom-right corner
            selectionBounds[3] = new object[] { x, y + SymbolSetHeight }; // bottom-left corner

            // Now that we have calculated the correct selection
            // box, select the region on the magenta layer, and
            // clear it, so it is no longer magenta.
            _currentDoc.ActiveLayer = _magentaBackgroundLayer;
            _currentDoc.Selection.Select(selectionBounds, PsSelectionType.psReplaceSelection, 0, false);
            _currentDoc.Selection.Clear();
        }

        /// <summary>
        /// Draws the "space" character. This character is handled differently
        /// from all other symbols, since it's bounds are zero when drawn in
        /// photoshop (it takes up no visible space when drawn alone).
        /// </summary>
        /// <param name="drawPosition"></param>
        /// <returns>The width of the space character.</returns>
        public static int DrawSpaceCharacter(Point drawPosition)
        {
            // Determine the width of our "space" character
            double spaceWidth = PhotoshopFontHelpers.CalculateSpaceCharacterWidth(_fontCopyLayer);

            // We don't actually have to draw any text. The space character is 
            // blank. All we need to do is clear the magenta background around
            // where the space character should be.
            int x = (int)drawPosition.X;
            int y = (int)drawPosition.Y;

            object[] selectionBounds = new object[4];
            selectionBounds[0] = new object[] { x, y }; // top-left corner
            selectionBounds[1] = new object[] { x + spaceWidth, y }; // top-right corner
            selectionBounds[2] = new object[] { x + spaceWidth, y + SymbolSetHeight }; // bottom-right corner
            selectionBounds[3] = new object[] { x, y + SymbolSetHeight }; // bottom-left corner

            // Now that we have calculated the correct selection
            // box, select the region on the magenta layer, and
            // clear it, so it is no longer magenta.
            _currentDoc.ActiveLayer = _magentaBackgroundLayer;
            _currentDoc.Selection.Select(selectionBounds, PsSelectionType.psReplaceSelection, 0, false);
            _currentDoc.Selection.Clear();

            return (int)Math.Round(spaceWidth);
        }

        /// <summary>
        /// Performs a final canvas expand. It's possible, if the font is exactly
        /// the right size, that one or more of our characters will line up exactly
        /// on the right edge of the canvas. While I don't think this will actually
        /// break XNA, I want to be safe and make sure all characters are always
        /// surrounded by magenta.
        /// </summary>
        public static void FinalExpand()
        {
            int oldCanvasWidth = (int)_currentDoc.Width;
            int oldCanvasHeight = (int)_currentDoc.Height;

            // Just add a little buffer to the right and bottom edges
            _currentDoc.ResizeCanvas(
                _currentDoc.Width + Globals.BufferSpaceBetweenCharacters,
                _currentDoc.Height + Globals.BufferSpaceBetweenCharacters,
                PsAnchorPosition.psTopLeft);

            FillNewCanvasArea(oldCanvasWidth, oldCanvasHeight);
        }

        /// <summary>
        /// Cleans up or closes the current document. If the user has selected
        /// to keep the document open in photoshop, layers will be merged and
        /// unnecessary layers will be deleted. Otherwise, the document will
        /// be closed without saving.
        /// </summary>
        /// <param name="keepPhotoshopDocumentsOpen"></param>
        public static void CleanUp(bool keepPhotoshopDocumentsOpen)
        {
            if (keepPhotoshopDocumentsOpen)
            {
                // Merge all character layers into one. This will remove the
                // layer set, and leave you with a single layer.
                _characterLayerSet.Merge();

                // Deselect everything
                _currentDoc.Selection.Deselect();

                // Delete the font and font copy layers. They are no longer
                // needed, our font is fully built
                _fontLayer.Delete();
                _fontCopyLayer.Delete();

                // For some reason, merging or deleting layers causes my black
                // background to become visible again. Weird.
                HideBlackReadabilityBackgroundLayer();
            }
            else
            {
                // Close the document without saving
                _currentDoc.Close(PsSaveOptions.psDoNotSaveChanges);
            }
        }

        /// <summary>
        /// Should be called after all font files have been built. This will
        /// close our template file and close photoshop if desired.
        /// </summary>
        /// <param name="keepTemplateDocumentOpen"></param>
        /// <param name="keepPhotoshopOpen"></param>
        public static void FinalCleanup(bool keepTemplateDocumentOpen, bool keepPhotoshopOpen)
        {
            // Close the template document without saving if desired
            if (!keepTemplateDocumentOpen)
                _templateDoc.Close(PsSaveOptions.psDoNotSaveChanges);

            // Close photoshop if desired. If not, make sure we set it back
            // to visible (if it was set to hidden for the build)
            if (!keepPhotoshopOpen)
                _ps.Quit();
            else
                ShowPhotoshop();
        }

        /// <summary>
        /// Saves the current document to disk. The name of the file will
        /// be "fontName-fontSize.png".
        /// </summary>
        /// <param name="d"></param>
        /// <param name="fontSize"></param>
        /// <param name="overwrite"></param>
        public static void SaveDocument(DirectoryInfo d, int fontSize, bool overwrite)
        {
            // Create the file name of the .png we will save
            string fileName = d.FullName + "\\" + GetFontName() + "-" + fontSize;

            // Save the file as a .png, with optional overwrite
            PhotoshopSaveHelpers.SaveForWeb(
                _currentDoc, 
                fileName, 
                PsSaveDocumentType.psPNGSave, 
                overwrite);
        }

        /// <summary>
        /// Shows the photoshop window. Even if the HidePhotoshopWhileBuilding flag is
        /// set, we still want to make photoshop visible when paused and when the 
        /// build is fully completed.
        /// </summary>
        public static void ShowPhotoshop()
        {
            // Don't just set the value. If photoshop is performing another task
            // (for example, the user is modifying text or styles), chaning the
            // visibility will throw an error stating that photoshop is busy.
            if (!IsPhotoshopVisible)
            {
                _ps.Visible = true;
                IsPhotoshopVisible = true;
            }
        }

        /// <summary>
        /// Hides the photoshop window. This is used when the HidePhotoshopWhileBuilding
        /// flag is set.
        /// </summary>
        public static void HidePhotoshop()
        {
            if (IsPhotoshopVisible)
            {
                _ps.Visible = false;
                IsPhotoshopVisible = false;
            }
        }

        /// <summary>
        /// Returns the currently selected font. Be careful when calling this.
        /// You will get an exception if Cleanup() has already been called, and
        /// your font layer was deleted.
        /// </summary>
        /// <returns>The font name for the currently selected font item.</returns>
        public static string GetFontName()
        {
            return _fontLayer.TextItem.Font;
        }

        /// <summary>
        /// Hides our font layers. They were used for calculating
        /// character dimensions and for the user to adjust settings
        /// and styles. We don't need them for the draw phase.
        /// </summary>
        public static void HideFontLayers()
        {
            _fontLayer.Visible = false;
            _fontCopyLayer.Visible = false;
        }

        /// <summary>
        /// Hides the black background layer in our current document. This layer
        /// is only for human readability and should not be included in the final
        /// saved image.
        /// </summary>
        public static void HideBlackReadabilityBackgroundLayer()
        {
            _blackReadabilityBackgroundLayer.Visible = false;
        }

        /// <summary>
        /// Check to see if we need to expand the canvas size. Drawing outside of the
        /// canvas is ok, but clearing a selection outside of the canvas will result in an
        /// error. Thus, we have to do a "Reveal All" to make sure our characters are 
        /// always correctly shown. 
        /// ***NOTE: there is a "Reveal All" bug in photoshop, which only occurs when 
        /// TextLayers are located beneath the layer you are trying to reveal all on. Be
        /// very careful that text layers are located on top of standard art layers. For
        /// more info, check out my google sites page:
        /// https://sites.google.com/site/ssbgame/photoshop-automation/reveal-all-bug
        /// </summary>
        private static void ExpandCanvasIfNecessary()
        {
            int buffer = Globals.BufferSpaceBetweenCharacters;

            // Keep track of the current canvas width before expansion
            int oldCanvasWidth = (int)_currentDoc.Width;
            int oldCanvasHeight = (int)_currentDoc.Height;

            // Expand the canvas just enough to reveal all elements
            _currentDoc.RevealAll();

            // If the canvas was expanded, we want to add a little additional
            // buffer space around the edges. RevealAll() only expands enough
            // to JUST barely fit all elements.
            if (_currentDoc.Width > oldCanvasWidth)
            {
                // Only add a buffer on the right side
                _currentDoc.ResizeCanvas(
                    _currentDoc.Width + buffer,
                    _currentDoc.Height,
                    PsAnchorPosition.psTopLeft);
            }

            if (_currentDoc.Height > oldCanvasHeight)
            {
                // Only add a buffer on the bottom
                _currentDoc.ResizeCanvas(
                    _currentDoc.Width,
                    _currentDoc.Height + buffer,
                    PsAnchorPosition.psTopLeft);
            }

            FillNewCanvasArea(oldCanvasWidth, oldCanvasHeight);
        }

        /// <summary>
        /// Any time we expand our canvas, we need to make sure to fill in
        /// the new areas with our background magenta color. 
        /// </summary>
        /// <param name="oldWidth"></param>
        /// <param name="oldHeight"></param>
        private static void FillNewCanvasArea(double oldWidth, double oldHeight)
        {
            double width = _currentDoc.Width;
            double height = _currentDoc.Height;
            object[] selectionBounds = new object[4];

            if (width > oldWidth)
            {
                // Select the new area that was added to the right of our canvas
                selectionBounds[0] = new object[] { oldWidth, 0 }; // top-left corner
                selectionBounds[1] = new object[] { width, 0 }; // top-right corner
                selectionBounds[2] = new object[] { width, height }; // bottom-right corner
                selectionBounds[3] = new object[] { oldWidth, height }; // bottom-left corner

                // Select the new region
                _currentDoc.Selection.Select(selectionBounds, PsSelectionType.psReplaceSelection, 0, false);

                // Fill the magenta layer
                _currentDoc.ActiveLayer = _magentaBackgroundLayer;
                _currentDoc.Selection.Fill(_magenta);

                // Fill the black layer
                _currentDoc.ActiveLayer = _blackReadabilityBackgroundLayer;
                _currentDoc.Selection.Fill(_black);
            }

            if (height > oldHeight)
            {
                // Select the new area that was added to the bottom of our canvas
                selectionBounds[0] = new object[] { 0, oldHeight }; // top-left corner
                selectionBounds[1] = new object[] { width, oldHeight }; // top-right corner
                selectionBounds[2] = new object[] { width, height }; // bottom-right corner
                selectionBounds[3] = new object[] { 0, height }; // bottom-left corner

                // Select the new region
                _currentDoc.Selection.Select(selectionBounds, PsSelectionType.psReplaceSelection, 0, false);

                // Fill the magenta layer
                _currentDoc.ActiveLayer = _magentaBackgroundLayer;
                _currentDoc.Selection.Fill(_magenta);

                // Fill the black layer
                _currentDoc.ActiveLayer = _blackReadabilityBackgroundLayer;
                _currentDoc.Selection.Fill(_black);
            }
        }

        /// <summary>
        /// Add line breaks after a certain number of characters. This will
        /// make long symbol sets more readable for the user. It will now
        /// be in more of a square form than one long line.
        /// </summary>
        /// <param name="contents"></param>
        /// <returns>The properly formatted string.</returns>
        private static string PrepareFontConents(string contents)
        {
            StringBuilder b = new StringBuilder();
            int count = 0;

            foreach (char c in contents)
            {
                // Add a line break every few characters
                if (count == Globals.LineBreakAfterCharacterCount)
                {
                    count = 0;

                    // A special character photoshop uses to indicate 
                    // a line break
                    b.Append("\u000D");
                }

                b.Append(c);
                count++;
            }

            return b.ToString();
        }

        #region Unused/Deprecated

        /// <summary>
        /// <para>
        /// ***NOTE*** This method is no longer used. However, it is a good example
        /// as to how we can load and execute javascript files in photoshop. 
        /// </para>
        /// <para>
        /// Initially, my entire font builder script was was written in javascript,
        /// as it was easy to write and test the entire script from within the 
        /// "Adobe ExtendScript Toolkit" and not have to worry about any interop problems. 
        /// However, this restricted me because it was not easy to get callbacks into 
        /// c# whenever a task was completed within photoshop. If I wanted to pause 
        /// execution or calculate current progress, I had to make multiple small 
        /// javascript calls from within c# loops so that I could allow for a pause or
        /// to update the current progress UI after each small task was completed. 
        /// Thus, more and more code started moving over to c#, and eventually I 
        /// eliminated the javascript altogther.
        /// </para>
        /// </summary>
        // ReSharper disable once UnusedMember.Local
        private static void LoadPhotoshopScripts()
        {
            // Load the photoshop script(s) that we want to run on each .psd file.
            // Keep in mind that these are pack uris - in our case they are pointers
            // to embedded resources. These files do not exist separately on the file
            // system, they are a part of the executable. So don't expect to be able
            // to get an absolute file system path to the script.
            Uri json2Uri = new Uri("pack://application:,,,/Content/Scripts/json2.js");
            Uri headerUri = new Uri("pack://application:,,,/Content/Scripts/header.jsx");
            Uri uri = new Uri("pack://application:,,,/Content/Scripts/font_builder.jsx");
            Uri buildSymbolDataUri = new Uri("pack://application:,,,/Content/Scripts/build_symbol_data.jsx");
            Uri drawSymbolUri = new Uri("pack://application:,,,/Content/Scripts/draw_symbol.jsx");

            // Create and save references to our header script and our json2 script. 
            // We will include these with every single javascript call to photoshop.
            // ReSharper disable UnusedVariable
            string headerScript = WpfResourceLoader.LoadResource<string>(headerUri, ResourceLoaderContentType.Text);
            string json2Script = WpfResourceLoader.LoadResource<string>(json2Uri, ResourceLoaderContentType.Text);
            string oldMassiveCodeFileScript = WpfResourceLoader.LoadResource<string>(uri, ResourceLoaderContentType.Text);
            string buildSymbolDataScript = WpfResourceLoader.LoadResource<string>(buildSymbolDataUri, ResourceLoaderContentType.Text);
            string drawSymbolScript = WpfResourceLoader.LoadResource<string>(drawSymbolUri, ResourceLoaderContentType.Text);
            // ReSharper restore UnusedVariable
        }

        /// <summary>
        /// <para>
        /// ***NOTE*** This method is no longer used. However, it is a good example
        /// as to how we can load and execute javascript files in photoshop. 
        /// </para>
        /// <para>
        /// Ideally, we could run ps.DoJavascriptFile(fileName), which would open
        /// the entire javascript file inside of photoshop. We could then call
        /// ps.DoJavascript(methodName), and run methods on the open file. However,
        /// because I want to keep the FontBuilder as one single executable, without
        /// external scripts that can be modified by users, it's impossible for me
        /// to use the ps.DoJavascriptFile(fileName) method.
        /// </para>
        /// <para>
        /// Because of this restriction, I can only use ps.DoJavascript(codeToExecute),
        /// which means I have to pass in all methods, variables, parameters, etc
        /// each time I execute a method. It's a little bit of a pain, but it works.
        /// </para>
        /// <para>
        /// Also, keep in mind that in order to truly make the font builder into
        /// a single .exe file, I will have to merge all external dll references
        /// into the exe itself using something like ILMerge:
        /// http://stackoverflow.com/questions/189549/embedding-dlls-in-a-compiled-executable
        /// </para>
        /// </summary>
        /// <param name="ps"></param>
        /// <param name="methodName"></param>
        /// <param name="methodScript"></param>
        /// <param name="json2Script"></param>
        /// <param name="headerScript"></param>
        /// <param name="args"></param>
        /// <returns>The value returned from photoshop.</returns>
        // ReSharper disable once UnusedMember.Local
        private static string ExecutePhotoshopJavascriptMethod(
            Photoshop.Application ps, 
            string methodName, 
            string methodScript, 
            string json2Script, 
            string headerScript, 
            string args)
        {
            // Build the javascript string to execute. This will be the
            // method call and it's parameters followed by the full script,
            // and any header files or additional scripts we might need, like
            // json2.js.
            string s = "";

            // Always include our header script and json2.js
            s += json2Script + "\n\n";
            s += headerScript + "\n\n";

            // Next, add the method call itself, and the script that contains
            // the method we want to execute. Include any method arguments 
            // here as well.
            s += methodName + "('" + EscapeJavascriptSpecialCharacters(args) + "');\n\n";
            s += methodScript;

            // Execute the method. Remember that the return value will be the
            // las value of the jsx file we execute. This value could either be:
            //  - The return value of a function we call OR
            //  - The last value at the bottom of the jsx file (not a return value)
            // See this page, and my javascript comments for more info:
            // http://stackoverflow.com/questions/3846626/is-it-possible-to-execute-jsx-scripts-from-outside-extendscript
            string returnValue = ps.DoJavaScript(s);

            // Keep the return value separate. Don't call "return ps.DoJavascript(s)".
            // This has caused some weird issues where the program got stuck and had
            // to be terminated. I think it's because we're trying to return a value
            // that has to wait for an unspecified period of time. Could have been
            // a fluke. Could have been because I was setting a breakpoint on the return
            // line. Who knows. Leave it like this to avoid issues.
            return returnValue;
        }

        /// <summary>
        /// <para>
        /// ***NOTE*** This method is no longer used. However, it is a good example
        /// for the future.
        /// </para>
        /// <para>
        /// Escapes special javascript characters. There are only a few that
        /// i'm really interested in:
        ///     \ (backslash)
        ///     " (double quote), 
        ///     ' (single quote)
        /// </para>
        /// For a full list, see here:
        /// http://msdn.microsoft.com/en-us/library/ie/2yfce773(v=vs.94).aspx
        /// </summary>
        /// <param name="s"></param>
        /// <returns>The escaped javascript string.</returns>
        private static string EscapeJavascriptSpecialCharacters(string s)
        {
            StringBuilder sb = new StringBuilder();

            foreach (char c in s)
            {
                if (c == '\\' || c == '\"' || c == '\'')
                    sb.Append("\\" + c);
                else
                    sb.Append(c);
            }

            return sb.ToString();
        }

        #endregion
    }
}
