﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Globals.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace FontBuilder
{
    using System;
    using System.Linq;

    /// <summary>
    /// Class definition for Globals.cs.
    /// </summary>
    public static class Globals
    {
        /// <summary>
        /// When displaying a graphical timer, I always want to show the
        /// minutes and seconds, but I don't want to show the hours unless
        /// they are necessary. This isn't possible using a single format
        /// string as far as I can tell, so I have to use two.
        /// Examples:
        /// 00:15       (15 secs - don't show hours)
        /// 10:15       (10 min, 15 secs - don't show hours)
        /// 01:14:22    (1 hour, 14 min, 22 secs)
        /// </summary>
        public const string LessThanOneHourDateFormat = "{0:mm\\:ss}";
        public const string MoreThanOneHourDateFormat = "{0:hh\\:mm\\:ss}";

        /// <summary>
        /// A delay used to ensure the user gets to see graphical status 
        /// messages before they dissapear.
        /// </summary>
        public const int ReadingDelay = 750;

        /// <summary>
        /// A small delay before photoshop commands to ensure that photoshop
        /// does not choke and become unresponsive. You can turn this option
        /// off by unchecking a value on the user interface.
        /// </summary>
        public const int PhotoshopResponsivenessDelay = 200;

        /// <summary>
        /// The starting width for photoshop documents. The documents will
        /// be resized automatically if they need to be bigger as the font
        /// is built.
        /// </summary>
        public const int StartingCanvasWidth = 400;

        /// <summary>
        /// The starting height for photoshop documents. The documents will
        /// be resized automatically if they need to be bigger as the font
        /// is built.
        /// </summary>
        public const int StartingCanvasHeight = 250;

        /// <summary>
        /// When drawing each character in photoshop, there needs to be
        /// some magenta colored spacing between each symbol, so that
        /// XNA can read it. There needs to be spacing around the top,
        /// left, bottom, and right edges as well. This defines exactly
        /// how much we want. 
        /// </summary>
        public const int BufferSpaceBetweenCharacters = 25;

        /// <summary>
        /// When building a font, we create a line break after a certain 
        /// number of characters. This is simply to ensure that our font 
        /// bitmap is more square and easier for humans to read rather 
        /// than one really long line of text. This is unnecessary as 
        /// far as xna is concerned.
        /// </summary>
        public const int LineBreakAfterCharacterCount = 16;

        /// <summary>
        /// A list of common font sizes. This will be the default value
        /// for the list of sizes to build.
        /// </summary>
        public const string CommonFontSizes = "12, 16, 22";

        /// <summary>
        /// The default font we will use in photoshop. The user can change
        /// this value during the pause/modify stage.
        /// </summary>
        public const string DefaultFont = "LithosPro-Regular";

        /// <summary>
        /// <para>
        /// The default font size we will display in the template document.
        /// The template document is where the user will modify any settings
        /// during the pause/modify stage. 
        /// </para>
        /// <para>
        /// This value is NOT a size that will be built, it is simply the
        /// size we will display to him for editing. 
        /// </para>
        /// </summary>
        public const int DefaultTemplateFontSize = 20;
    }
}
