﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// <summary>
//   Interaction logic for MainWindow.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FontBuilder.Views
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using FontBuilder.ViewModels;
    using Sector12Common.Helpers;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainWindowViewModel _viewModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            _viewModel = new MainWindowViewModel();
            DataContext = _viewModel;

            // The dispatcher is null until after InitializeComponent()
            _viewModel.UIDispatcher = Dispatcher;

            // Retrieve the values the user provided last time they ran
            // the program. This is taken from the WPF Application Settings
            // (right click FontBuilder -> Properties -> Settings).
            // For more detail, see this discussion:
            // http://stackoverflow.com/questions/396229/wpf-c-where-should-i-be-saving-user-preferences-files
            _viewModel.SaveDirectory = Properties.Settings.Default.SaveDirectory;
        }

        /// <summary>
        /// Pay attention to when the application is terminating.
        /// This will happen if the user clicks the "exit" menu
        /// item, or if the user clicks the "x" in the top right.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            // Code put in by default when visual studio auto-generated
            // this method.
            base.OnClosing(e);

            // Make sure our worker thread is shut down before closing the app.
            // If we don't do this, the worker thread could attempt updating the
            // gui, which will no longer exist. This will obviously cause errors.
            // We don't want to sleep here however - if the worker thread does
            // attempt an Invoke() on the UI thread, it will never get processed,
            // because we are keeping the UI locked up with a sleep. In other words,
            // our worker thread will hang because it's waiting for the UI, and 
            // the UI will hang because it's waiting for the worker. Instead,
            // just tell the worker thread that we want to abort, and cancel the
            // close operation for now. Once the worker thread has aborted and
            // is about to terminate, it will call Application.Current.Shutdown()
            // on the dispatcher thread, and this method will be called once more.
            _viewModel.IsClosing = true;
            _viewModel.IsAborting = true;

            // If the worker thread is not finished, don't close the window.
            // Don't sleep either. The worker thread will trigger a shutdown
            // operation once it has finished executing, and this method will
            // be called one more time.
            if (_viewModel.IsExecuting)
                e.Cancel = true;
        }

        /// <summary>
        /// Resets all world builder data. Also resets data within the
        /// ViewModel. It does not reset any data specified by the user 
        /// (checkboxes, save location, etc).
        /// </summary>
        private void Reset()
        {
            // Reset GUI data
            _viewModel.Reset();
        }

        /// <summary>
        /// Displays the about dialog.
        /// </summary>
        private void ShowAboutDialog()
        {
            AboutDialog d = new AboutDialog();
            d.Owner = this;
            d.ShowDialog();
        }

        /// <summary>
        /// Ensures that our save directory exists.
        /// </summary>
        /// <returns>Returns a DirectoryInfo objects that is valid and exists.</returns>
        private DirectoryInfo GetAndVerifySaveDirectory()
        {
            // Make sure we center message boxes on our form
            MessageBoxHelper.PrepToCenterMessageBoxOnForm(this);

            // Make sure the user has entered some value
            if (string.IsNullOrEmpty(_viewModel.SaveDirectory))
            {
                MessageBox.Show(
                    "You must enter a value for \"Save Directory\".",
                    "Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return null;
            }

            // Create the directory info
            DirectoryInfo dir = new DirectoryInfo(_viewModel.SaveDirectory);

            // Verify that the directory exists
            if (!dir.Exists)
            {
                MessageBox.Show(
                    "Save directory \"" + dir.FullName + "\" does not exist.",
                    "Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return null;
            }

            // The save directory checked out. It has everything we need.
            return dir;
        }

        /// <summary>
        /// Occurs when the "Exit" menu item is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // Shut down the application. This will trigger the
            // "OnClosing()" event. 
            Application.Current.Shutdown();
        }

        /// <summary>
        /// Command execute permission.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            // Allow the user to exit the program at any time
            e.CanExecute = true;
        }

        /// <summary>
        /// Occurs when the "Build World" button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlayCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // Check to make sure the save directory exists
            DirectoryInfo saveDirectory = GetAndVerifySaveDirectory();
            if (saveDirectory == null)
                return;

            // Save the values into the WPF Application Settings so that they
            // can be used by default the next time the user runs this program.
            Properties.Settings.Default.SaveDirectory = saveDirectory.FullName;
            Properties.Settings.Default.Save();

            // Now that everything is verified, reset all build variables (timers, etc)
            Reset();

            // Display the progress overlay.
            _viewModel.IsAdornerVisible = true;
            ProgressOverlay control = (ProgressOverlay)ProgressOverlayAdorner.AdornerContent;
            control.MainWindowViewModel = _viewModel;

            // Start processing the request in a different thread
            Thread workerThread = new Thread(() => control.StartWorking(saveDirectory));
            workerThread.Start();
        }

        /// <summary>
        /// Command execute permission.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlayCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        /// <summary>
        /// MenuItem events bubble up, so I only have to subscribe my entire
        /// menu object to a single MenuItemClick event. All of the menu's 
        /// children will fire events that will be caught here as well.
        /// Just check the incoming event to see which menu item was the
        /// source of the event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            // I implemented commands on all buttons and menu items
            // except the about menu item after I wrote this code.
            // So now, there is only one item being used here. However,
            // I decided to leave this code as is as an example for
            // future projects.
            MenuItem item = e.OriginalSource as MenuItem;
            if (item == null)
                return;

            if (item.Equals(AboutMenuItem))
                ShowAboutDialog();
        }

        /// <summary>
        /// Popup a save location dialog.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveDirectoryButton_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.SaveDirectory = DialogHelpers.ChooseDirectory(_viewModel.SaveDirectory, this);
        }
    }
}
