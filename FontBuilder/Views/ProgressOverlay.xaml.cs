﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgressOverlay.xaml.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace FontBuilder.Views
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Threading;
    using FontBuilder.PhotoshopLogic;
    using FontBuilder.ViewModels;
    using Sector12Common.Controls.ControlData;

    /// <summary>
    /// Interaction logic for ProgressOverlay.xaml
    /// </summary>
    public partial class ProgressOverlay : UserControl
    {
        private DispatcherTimer _timer;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgressOverlay"/> class.
        /// </summary>
        public ProgressOverlay()
        {
            InitializeComponent();

            // Setup our ProgressBars for use from a worker thread. This way, we
            // can update our progress bars without having to make sure every
            // call we make is executed on the dispatcher thread.
            ProgressBars.InitializeForUseInWorkerThread(Dispatcher, Globals.ReadingDelay);

            // Set up our time elapsed timer.
            _timer = new DispatcherTimer();
            _timer.Interval = TimeSpan.FromMilliseconds(1000);
            _timer.Tick += Timer_Tick;
        }

        /// <summary>
        /// Gets or sets our primary ViewModel. Ties data to the front end via binding.
        /// </summary>
        public MainWindowViewModel MainWindowViewModel { get; set; }

        /// <summary>
        /// Starts executing the build. This method should be called from within
        /// a separate thread.
        /// </summary>
        /// <param name="saveDirectory"></param>
        public void StartWorking(DirectoryInfo saveDirectory)
        {
            // Reset everything and start timers, etc.
            UI(() => BuildStarted());

            // Populate our progress bars with tasks
            UI(() => InitializeProgressBars());

            // Count the total number of fonts we want to build
            IEnumerable<int> sizesToBuild = MainWindowViewModel.GetSizesToBuildList();

            // Show our progress bars and hide our "it's time to modify photoshop" message
            UI(() => ProgressBars.Visibility = Visibility.Visible);
            UI(() => ItsTimeToModifyPhotoshopTextBlock.Visibility = Visibility.Collapsed);

            // Open photoshop if it's not already running
            ExecuteTask(() => PhotoshopCalls.InitializePhotoshop());

            // Create a string that contains all of the characters/symbols
            // we want to build.
            string symbolsToBuild = null;
            ExecuteTask(() => symbolsToBuild = LoadSymbolsToBuildString());

            // Open and configure a new template document that we will use as a base
            // for creating each font size that we have specified. The font size of
            // the template can be anything, but should be a size that is easily
            // readable for editing.
            ExecuteTask(() => PhotoshopCalls.CreateTemplatePhotoshopDocument(symbolsToBuild));

            // Set the application to paused. At this point, photoshop is open,
            // and a template document is open with all of the requested symbols.
            // Now, the user has an opportunity to make changes in photoshop
            // before building the font. Show him a message along these lines.
            // Hide our progress bars while this is happening.
            UI(() => ItsTimeToModifyPhotoshopTextBlock.Visibility = Visibility.Visible);
            UI(() => ProgressBars.Visibility = Visibility.Collapsed);
            MainWindowViewModel.HasPhotoshopBeenInitialized = true;

            // Pause. If the user aborts during the pause, clean up and exit
            // our thread.
            MainWindowViewModel.IsPaused = true;
            CheckForPause();
            if (MainWindowViewModel.IsAborting)
            {
                BuildCompleted();
                return;
            }

            // Now that the user has finished modifying photoshop styles, show our
            // progress bars and hide our "it's time To modify photoshop" message.
            UI(() => ItsTimeToModifyPhotoshopTextBlock.Visibility = Visibility.Collapsed);
            UI(() => ProgressBars.Visibility = Visibility.Visible);

            // Save the font name the user selected for later use. After the
            // font is built and Cleanup() is called, this won't be available
            // because we will delete our temporary font layers.
            string fontName = PhotoshopCalls.GetFontName();

            // Prep our output directory and master photoshop document
            ExecuteTask(() => PrepForBuild(saveDirectory, fontName));

            // Now start building the font. Create one new document for
            // each font size.
            foreach (int size in sizesToBuild)
            {
                // Create a new document
                int closureSafeSize = size;
                ExecuteTask(() => PhotoshopCalls.CreateFontBuilderPhotoshopDocument(closureSafeSize), "Creating Document...");

                // Build symbol set data, one at a time, each as a separate task
                BuildSymbolSetData(symbolsToBuild);

                // Calculate vertical offsets
                ExecuteTask(() => PhotoshopCalls.CalculateVerticalOffsets(), "Calculating Vertical Offsets...");

                // Calculate the total height of our symbol set (the top of the
                // highest element to the bottom of the lowest hanging element)
                ExecuteTask(() => PhotoshopCalls.CalculateSymbolSetHeight(), "Calculating Symbol Set Height...");

                // Draw each symbol, each as a separate task
                DrawAllSymbols();

                // Perform some final wrap up activities and save the document
                ExecuteTask(() => FinalizeAndSave(saveDirectory, closureSafeSize), "Finalizing and Saving .png...");

                // Merge and delete unnecessary layers.
                ExecuteTask(() => PhotoshopCalls.CleanUp(MainWindowViewModel.KeepPhotoshopDocumentsOpen), "Merging Layers...");

                // We successfully built another font
                MainWindowViewModel.TotalFontsCreated++;
            }

            // Finally, create a .spritefont file if we built anything other than
            // common english symbols. XNA will use this file to understand what
            // characters we are using.
            if (MainWindowViewModel.CreateSpritefontFile)
            {
                ExecuteTask(() =>
                    SpritefontFileBuilder.BuildSpritefontFile(
                    saveDirectory,
                    fontName,
                    MainWindowViewModel.Overwrite));
            }

            // Update our progress bars to show they are complete
            UI(() => ProgressBars.FinalTask("Build Complete!"));
            Thread.Sleep(Globals.PhotoshopResponsivenessDelay);

            // Stop all of our timers, update the build result
            BuildCompleted();
        }

        /// <summary>
        /// Prepares the output directory and master photoshop document.
        /// </summary>
        /// <param name="saveDirectory"></param>
        /// <param name="fontName"></param>
        private void PrepForBuild(DirectoryInfo saveDirectory, string fontName)
        {
            // Cleanup old files in the save directory if desired
            if (MainWindowViewModel.Overwrite)
                CleanupSaveDirectory(saveDirectory, fontName);

            // Also create a secondary font layer, which is an exact copy of
            // the original font layer, that can be used for drawing, determining 
            // individual character sizes and bounds, etc. without harming the
            // original layer.
            PhotoshopCalls.CreateFontCopyLayer();

            // First, hide the font layers. They were used for calculating
            // character dimensions and for the user to adjust settings
            // and styles. We don't need them for the draw phase.
            PhotoshopCalls.HideFontLayers();
        }

        /// <summary>
        /// Performs some final wrap up activities and saves the document.
        /// </summary>
        /// <param name="saveDirectory"></param>
        /// <param name="fontSize"></param>
        private void FinalizeAndSave(DirectoryInfo saveDirectory, int fontSize)
        {
            // Perform a final canvas expand, just in case any of our characters 
            // are lined up perfectly flush with the right or bottom edges.
            PhotoshopCalls.FinalExpand();

            // Hide the black background layer before we save. We just want it for 
            // readability. We don't want it to appear in the final saved image.
            PhotoshopCalls.HideBlackReadabilityBackgroundLayer();

            // Save the document to the save directory
            PhotoshopCalls.SaveDocument(saveDirectory, fontSize, MainWindowViewModel.Overwrite);
        }

        /// <summary>
        /// Populates our progress bars with tasks.
        /// </summary>
        private void InitializeProgressBars()
        {
            string symbolsToBuild = LoadSymbolsToBuildString();
            IEnumerable<int> sizesToBuild = MainWindowViewModel.GetSizesToBuildList();

            // Add some basic primary tasks
            ProgressBars.Tasks.Add(new ProgressBarTask("Initializing Photoshop...", "", 0));
            ProgressBars.Tasks.Add(new ProgressBarTask("Loading List of Symbols to Build...", "", 0));
            ProgressBars.Tasks.Add(new ProgressBarTask("Creating Template Document...", "", 0));
            ProgressBars.Tasks.Add(new ProgressBarTask("Preparing For Build...", "", 0));

            // Populate our progress bars with build tasks and a subtask count. 
            // We will set the subtask text later, on the fly. We want two subtasks 
            // for each symbol we have to draw (one for building symbol data, and 
            // one for drawing). We also want a few additional sub tasks for 
            // "creating photoshop document", "calculating vertical offsets", etc.
            int additionalSubTasksPerFontSize = 5;
            foreach (int size in sizesToBuild)
            {   
                ProgressBars.Tasks.Add(new ProgressBarTask(
                    "Building Font Size " + size + "...",
                    "",
                    (symbolsToBuild.Length * 2) + additionalSubTasksPerFontSize));
            }

            // Add one more task for creating a .spritefont file if configured to do so
            if (MainWindowViewModel.CreateSpritefontFile)
                ProgressBars.Tasks.Add(new ProgressBarTask("Creating .spritefont file...", "", 0));

            // Show our progress bars
            UI(() => ProgressBars.Visibility = Visibility.Visible);
        }

        /// <summary>
        /// Checks to see if the pause button has been pressed by the user. 
        /// If it has, it puts the worker thread to sleep and waits for the 
        /// resume button to be pressed.
        /// </summary>
        private void CheckForPause()
        {
            // If we are paused, check every second or so to see if we 
            // should resume our work.
            while (MainWindowViewModel.IsPaused && !MainWindowViewModel.IsAborting)
            {
                // If timers are already stopped, nothing happens
                StopTimers();

                // Make sure photoshop is always visible when paused. If we've set
                // it to be hidden while building, we want to re-show it now.
                PhotoshopCalls.ShowPhotoshop();

                // Sleep for a little bit, and then check again to see if
                // we're unpaused.
                Thread.Sleep(500);

                // Update the status after the short reading delay. This way,
                // the user always has time to see the longer "pausing... " 
                // message before switching text to "PAUSED".
                MainWindowViewModel.Status = "PAUSED";
            }

            // If we're done pausing, and we're not aborting, we can set photoshop
            // to hidden again if desired.
            if (!MainWindowViewModel.IsAborting && MainWindowViewModel.HidePhotoshopWhileBuilding)
                PhotoshopCalls.HidePhotoshop();

            // Re-start our timers
            StartTimers();

            // Remove the "PAUSED" status message
            if (MainWindowViewModel.Status != null && MainWindowViewModel.Status.Equals("PAUSED"))
                MainWindowViewModel.Status = "";
        }

        /// <summary>
        /// Starts our GUI timer.
        /// </summary>
        private void StartTimers()
        {
            if (!_timer.IsEnabled)
                _timer.Start();
        }

        /// <summary>
        /// Stops our GUI timer.
        /// </summary>
        private void StopTimers()
        {
            if (_timer.IsEnabled)
                _timer.Stop();
        }

        /// <summary>
        /// Build necessary data about each symbol or character, such
        /// as it's dimensions, bounds, vertical offset, etc.
        /// </summary>
        /// <param name="symbolsToBuild"></param>
        private void BuildSymbolSetData(string symbolsToBuild)
        {
            // Build necessary data about each symbol or character, such
            // as it's dimensions, bounds, vertical offset, etc.
            foreach (char c in symbolsToBuild)
            {
                //Create the symbol data
                char closureSafeChar = c;
                ExecuteTask(
                    () => 
                    PhotoshopCalls.BuildSymbolData(closureSafeChar), 
                    "Building Symbol Data: '" + closureSafeChar + "'");
            }
        }

        /// <summary>
        /// Draws all symbols using the symbold data calculated earlier.
        /// </summary>
        private void DrawAllSymbols()
        {
            int lineBreakCount = 0;
            int buffer = Globals.BufferSpaceBetweenCharacters;

            // Create our draw position variable. After each symbol is drawn,
            // we will increment the x and/or y values, so the next symbol
            // is drawn in a different location.
            Point currentDrawPosition = new Point(buffer, buffer);

            // Draw our "space" character
            int spaceCharacterWidth = PhotoshopCalls.DrawSpaceCharacter(currentDrawPosition);

            // Increment our draw position and line break variables after drawing
            // the "space" character. Space is always the first character, so don't
            // worry about the vertical draw position.
            currentDrawPosition.X += spaceCharacterWidth + buffer;
            lineBreakCount++;

            // Draw all other symbols
            foreach (FontSymbolData data in PhotoshopCalls.SymbolData)
            {
                // Move on to the next subtask. Draw the character.
                // Make sure to create a local copy of the foreach variable first so
                // that we can avoid resharper's warning about accessing foreach 
                // variables in a closure.
                FontSymbolData closureSafeData = data;
                string displayText = "Building Symbol: '" + data.Character + "'";
                ExecuteTask(() => PhotoshopCalls.DrawSymbol(closureSafeData, currentDrawPosition), displayText);

                // Increment our character counter
                lineBreakCount++;

                // Increment the paste location variable. This determines where 
                // the next character will be placed in the document after we 
                // paste it in. Create a line break after a certain number of characters. 
                // This is simply to ensure that our font bitmap is more square 
                // and easier for humans to read rather than one really long line 
                // of text. This is unnecessary as far as xna is concerned.
                if (lineBreakCount >= Globals.LineBreakAfterCharacterCount)
                {
                    currentDrawPosition.X = buffer;
                    currentDrawPosition.Y += PhotoshopCalls.SymbolSetHeight + buffer;
                    lineBreakCount = 0;
                }
                else
                {
                    currentDrawPosition.X += data.Width + buffer;
                }

                // Use a slight delay to prevent photoshop from choking and 
                // becoming unresponsive
                if (MainWindowViewModel.UseDelayForPhotoshopResponsiveness && !MainWindowViewModel.HidePhotoshopWhileBuilding)
                    Thread.Sleep(Globals.PhotoshopResponsivenessDelay);
            }
        }

        /// <summary>
        /// Should be called before the build begins. This resets all progress bar
        /// and view model values, starts our timers, etc.
        /// </summary>
        private void BuildStarted()
        {
            // Reset everything
            MainWindowViewModel.Reset();
            ProgressBars.Reset(true);

            // Dave can start humping
            ToddImage.Play();

            // Start our timers
            StartTimers();
            MainWindowViewModel.IsExecuting = true;
            MainWindowViewModel.HasPhotoshopBeenInitialized = false;
        }

        /// <summary>
        /// Executes a single task and calls the NextTask() method on the progress
        /// bars control. This method will also check for any pauses or aborts before
        /// executing the given task.
        /// </summary>
        /// <param name="methodToExecute"></param>
        /// <param name="subtaskName"></param>
        /// <param name="subtaskDetails"></param>
        private void ExecuteTask(Action methodToExecute, string subtaskName = "", string subtaskDetails = "")
        {
            // Check for a pause or an abort. Wait if the application is paused,
            // and return if the user has aborted the build.
            CheckForPause();
            if (MainWindowViewModel.IsAborting)
                return;

            // Update the progress bars
            ProgressBars.NextTask(subtaskName, subtaskDetails);

            // Execute the method
            methodToExecute();

            // For debugging
            //ProgressBars.PrintTotalTaskDebugInfo();
        }

        /// <summary>
        /// Code that we always want to run after our build has completed or
        /// been aborted.
        /// </summary>
        private void BuildCompleted()
        {
            // We've either successfully completed or aborted our build,
            // so hide our progress bars. If the user clicked the close
            // or exit button however, don't update things on the ui dispatcher
            // anymore. It will cause the application to hang.
            if (!MainWindowViewModel.IsClosing)
                UI(() => ProgressBars.Visibility = Visibility.Collapsed);

            // Update our build result label
            if (MainWindowViewModel.IsAborting)
                MainWindowViewModel.BuildResult = "Build Aborted";
            else
                MainWindowViewModel.BuildResult = "Build Successful";

            // Stop our timers
            StopTimers();
            MainWindowViewModel.IsExecuting = false;

            // Perform a final photoshop cleanup/close
            PhotoshopCalls.FinalCleanup(MainWindowViewModel.KeepPhotoshopDocumentsOpen, MainWindowViewModel.KeepPhotoshopOpen);

            // Todd can stop now
            UI(() => ToddImage.Pause());

            // If the user tried to close the application while the worker thread
            // was executing, we will have cancelled that operation. We didn't
            // want to close the window until the worker thread was completed.
            // Now that we're done, we want to close the app, like the user 
            // previously requested.
            if (MainWindowViewModel.IsClosing)
                UI(() => Application.Current.Shutdown());
        }

        /// <summary>
        /// Loads a string containing all of the symbols we want to build. 
        /// </summary>
        /// <returns>A string containing all of the symbols we want to build.</returns>
        private string LoadSymbolsToBuildString()
        {
            if (MainWindowViewModel.BuildKoreanHangul)
                return SymbolLoader.LoadKoreanHangul();
            else if (MainWindowViewModel.BuildExtendedASCIISymbols)
                return SymbolLoader.LoadExtendedASCIISymbols();
            else
                return SymbolLoader.LoadCommonEnglishSymbols();
        }

        /// <summary>
        /// Deletes old .pngs and .spritefont files containing the name of the
        /// font we are building. This will not delete old files for different
        /// font names.
        /// </summary>
        /// <param name="saveDirectory"></param>
        /// <param name="fontName"></param>
        private void CleanupSaveDirectory(DirectoryInfo saveDirectory, string fontName)
        {
            // Delete old png that start with our font name
            FileInfo[] pngFiles = saveDirectory.GetFiles(fontName + "*.png");
            foreach (FileInfo file in pngFiles)
            {
                file.Delete();
            }

            // Delete any old .spritefont files that start with our font name
            FileInfo[] spritefontFiles = saveDirectory.GetFiles(fontName + "*.spritefont");
            foreach (FileInfo file in spritefontFiles)
            {
                file.Delete();
            }
        }

        /// <summary>
        /// Executes the provided delegate on the UIDispatcher thread. The code
        /// used to call this function is pretty damn sexy and compact. Check out
        /// these discussions if you want to read more about how to call delegates
        /// using lamda expressions and anonymous functions.
        /// http://stackoverflow.com/questions/4621623/wpf-multithreading-ui-dispatcher-in-mvvm
        /// http://stackoverflow.com/questions/2082615/pass-method-as-parameter-using-c-sharp
        /// </summary>
        /// <param name="delegateAction"></param>
        private void UI(Action delegateAction)
        {
            MainWindowViewModel.UIDispatcher.Invoke(DispatcherPriority.Normal, delegateAction);
        }

        /// <summary>
        /// Timer tick event listener.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_Tick(object sender, EventArgs e)
        {
            // Increment the number of seconds passed
            MainWindowViewModel.SecondsElapsed++;
        }

        /// <summary>
        /// Button click event listener.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindowViewModel.IsAdornerVisible = false;
        }

        /// <summary>
        /// Occurs when either the "Resume" button or the "Pause" button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TogglePlayPauseCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // Don't start or stop the timers here. Let the worker thread do that.
            // When we hit pause, the worker is not paused immediately. It waits
            // until the current operation is completed. 
            if (MainWindowViewModel.IsPaused)
            {
                MainWindowViewModel.IsPaused = false;
                MainWindowViewModel.Status = "";
                UI(() => ToddImage.Play());
            }
            else
            {
                MainWindowViewModel.IsPaused = true;
                MainWindowViewModel.Status = "Pausing... Waiting for current operation to complete...";
                UI(() => ToddImage.Pause());
            }
        }

        /// <summary>
        /// Whether or not the "Pause/Resume" button can be clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TogglePlayPauseCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            // Only enable the pause/resume buttons if we're actually executing a crawl.
            // Also, don't allow the user to pause until photoshop has been initialized.
            if (MainWindowViewModel != null && MainWindowViewModel.IsExecuting && 
                !MainWindowViewModel.IsAborting && MainWindowViewModel.HasPhotoshopBeenInitialized)
                e.CanExecute = true;
            else
                e.CanExecute = false;
        }

        /// <summary>
        /// Occurs when the "Abort" button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StopCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // Tell the thread to gracefully stop itself if it is running
            MainWindowViewModel.IsAborting = true;

            // If we're already paused, we know the current operation 
            // is already completed, because pause waits for the current
            // operation to complete as well.
            if (!MainWindowViewModel.IsPaused)
                MainWindowViewModel.Status = "Aborting... Waiting for current operation to complete...";
        }

        /// <summary>
        /// Whether or not the "Abort" button can be clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StopCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            // Only enable the abort button if we're actually executing a crawl.
            if (MainWindowViewModel != null && MainWindowViewModel.IsExecuting && !MainWindowViewModel.IsAborting)
                e.CanExecute = true;
            else
                e.CanExecute = false;
        }
    }
}
