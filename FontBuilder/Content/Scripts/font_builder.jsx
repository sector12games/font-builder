﻿// !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_'abcdefghijklmnopqrstuvwxyz{|}~

var doc = app.activeDocument;
var count = 0;
var fontLayer;
var fontTempLayer;
var backgroundLayer;

var magentaColor = new SolidColor;
    magentaColor.rgb.red = 255;
    magentaColor.rgb.green = 0;
    magentaColor.rgb.blue = 255;
    
var blackColor = new SolidColor;
    blackColor.rgb.red = 0;
    blackColor.rgb.green = 0;
    blackColor.rgb.blue = 0;

app.preferences.rulerUnits = Units.PIXELS; 
app.preferences.typeUnits = TypeUnits.PIXELS; 
app.displayDialogs = DialogModes.NO;

// Our background layer isn't really a background layer. It's a transparent
// layer we created from within the FontBuilder program.
backgroundLayer = doc.artLayers[doc.artLayers.length - 1];

// Find the text layers we're interested in
for (var i = 0; i < doc.artLayers.length; i++)
{
    if (doc.artLayers[i].name == "Font")
        fontLayer = doc.artLayers[i];
}

if (fontLayer == null)
    alert("You must have a text layer named 'Font'");
else
    BuildBitmapFont();
    
    
    

    
    

function BuildBitmapFont()
{
    var symbolString = trim(fontLayer.textItem.contents);
    var tallestCharacter;
    var tallestCharacterHeight;
    var spaceCharacterWidth;
    
    var c; // character
    var r; // rect
    var tr; // translated-rect
    
    var selectionBounds;
    var newLayer;
    
    var buffer = 20; // spacing between characters
    var vb; // vertical buffer
    var currentPasteLocationX = buffer;
    var currentPasteLocationY = buffer;
    var translateX;
    var translateY;
    
    var oldCanvasWidth;
    var oldCanvasHeight;
    
    // Make sure the symbol set layer is visible before starting
    fontLayer.visible = true;
    
    // First, create a temporary text layer with all the same styles and properties
    // as our primary symbol-set layer
    fontTempLayer = fontLayer.duplicate();
    
    // Before we start, we need to calculate the tallest character in our symbol set.
    // Also calculate the width of the "space" character.
    tallestCharacter = determineHighestPositionedCharacter(symbolString);
    tallestCharacterHeight = calculateStringBoundsRect(tallestCharacter).height;
    spaceCharacterWidth = determineSpaceCharacterWidth();
    
    // Select all and apply a fill to the current selection
    doc.selection.selectAll();
    doc.activeLayer = backgroundLayer;
    doc.selection.fill(magentaColor);
    
    // The first character will always be a space. Because this character has no real content,
    // we need to draw it manually.
    drawSpaceCharacter (currentPasteLocationX, currentPasteLocationY, spaceCharacterWidth, tallestCharacterHeight);
    currentPasteLocationX += spaceCharacterWidth + buffer;
    
    for (var i = 0; i < symbolString.length; i++)
    {  
        // Deselect everything before we start
        doc.selection.deselect();
        
        // Put the single character into our temporary text layer
        c = symbolString.charAt(i);
        fontTempLayer.textItem.contents = c;
        
        // Copy the text layer, and paste back into photoshop as an art layer
        newLayer = fontTempLayer.duplicate();
        newLayer.rasterize (RasterizeType.TEXTCONTENTS);
        newLayer.move(doc, ElementPlacement.PLACEATEND);
        
        // Create a more usable "rect" object from the layer's bounds
        r = new rect(newLayer.bounds);
        
        // Calculate the vertical buffer. When layers are pasted in, they will be pasted in using
        // the top-left corner of their content. Some characters are taller than others, so we want
        // to make sure the characters are all vertically aligned along the center line, not the top
        // line. Essentially, this is setting our font builder to "VerticalAlign=Center". 
        vb = CalculateVerticalBuffer(tallestCharacter, c);
        
        // Moving layers in photoshop is done via a "translate" function. We cannot position
        // a layer absolutely, so we need to move it relative to where it currently is. Determine
        // how far we will move it vertically and horizontally, and move it.
        translateX = currentPasteLocationX - r.left;
        translateY = currentPasteLocationY - r.top + vb;
        newLayer.translate(translateX, translateY);
        
        // Now that we've translated our layer, we need to determine the new
        // rectangle at which our layer is located;
        tr = new rect(newLayer.bounds);
        
        // Now we need to select a square around the character we just pasted in. We need
        // to select the square, and clear it, so that the magenta background does not appear
        // where the character is located. This is how xna will determine what portions of the
        // canvas are characters and which portions of the canvas are transparent (magenta).
        selectionBounds = [
            [currentPasteLocationX, currentPasteLocationY], // top-left
            [currentPasteLocationX + tr.width, currentPasteLocationY], // top-right
            [currentPasteLocationX + tr.width, currentPasteLocationY + tallestCharacterHeight], // bottom-right
            [currentPasteLocationX, currentPasteLocationY + tallestCharacterHeight]]; // bottom-left
            
        // Check to see if we need to expand the canvas size. Drawing outside of the
        // canvas is ok, but clearing a selection outside of the canvas will result in an
        // error. Thus, we have to do a "Reveal All" to make sure our characters are 
        // always correctly shown.
        // ***NOTE: there is a "Reveal All" bug in photoshop, which only occurs when 
        // TextLayers are located beneath the layer you are trying to reveal all on. Be
        // very careful that text layers are located on top of standard art layers. For
        // more info, check out my google sites page:
        // https://sites.google.com/site/ssbgame/photoshop-automation/reveal-all-bug
        oldCanvasWidth = doc.width;
        oldCanvasHeight = doc.height;
        doc.revealAll();
        fillNewCanvasArea(oldCanvasWidth, oldCanvasHeight);
            
        // Now that we have selection coordinates, select the background layer and 
        // clear the rectangle we selected.
        doc.activeLayer = backgroundLayer;
        doc.selection.select(selectionBounds,SelectionType.REPLACE,0,false);
        doc.selection.clear();
        
        // Increment the paste location variables. This determines where the next
        // character will be placed in the document after we paste it in.
        // Create a line break after certain characters. This is simply to ensure that
        // our font bitmap is more square and easier for humans to read rather
        // than one really long line of text. This is unnecessary as far as xna is concerned.
        currentPasteLocationX += r.width + buffer;
        if (c == "/"  || c == "?" || c == "O" || c == "_" || c == "o") 
        {
            currentPasteLocationX = buffer;
            currentPasteLocationY += tallestCharacterHeight + buffer;
        }
    
        // Last, clear the contents of our fontTempLayer
        fontTempLayer.textItem.contents = "";
    }

    // Expand the canvas slightly to add an extra little buffer on the right and bottom edges.
    // This is not necessary, and is purely to make the image look better to the human eye.
    // This will really only be beneficial when the canvas has had to be resized via "reveal all".
    // Without this, characters will appear on the very edges of the image if a reveal all occured,
    // and I want them to be centered in the bitmap.
    oldCanvasWidth = doc.width;
    oldCanvasHeight = doc.height;
    doc.resizeCanvas(doc.width + buffer, doc.height + buffer, AnchorPosition.TOPLEFT);
    fillNewCanvasArea(oldCanvasWidth, oldCanvasHeight);

    // After everything is done and our bitmap is built, deselect everything and hide 
    // the original symbol set layer. Also delete the temporary font layer that we created
    // for copy/paste purposes. The canvas is now prepped for saving.
    fontTempLayer.remove();
    doc.selection.deselect();
    fontLayer.visible = false;
}

function drawSpaceCharacter(left, top, width, height)
{
    var selectionBounds = [
            [left, top], // top-left
            [left + width, top], // top-right
            [left + width, top + height], // bottom-right
            [left, top + height]]; // bottom-left
            
    doc.activeLayer = backgroundLayer;
    doc.selection.select(selectionBounds,SelectionType.REPLACE,0,false);
    doc.selection.clear();
}

function CalculateVerticalBuffer(tallestSymbol, symbol)
{
    var tallestSymbolBounds;
    var symbolBounds;
    var newLayer;
    
    // Create a new layer with only the tallest character. Convert the layer into a standard 
    // art layer, get it's bounds, and remove it
    fontTempLayer.textItem.contents = tallestSymbol;
    newLayer = fontTempLayer.duplicate();
    newLayer.rasterize (RasterizeType.TEXTCONTENTS);
    tallestSymbolBounds = newLayer.bounds;
    newLayer.remove();
    
    // Do the same for our other symbol
    fontTempLayer.textItem.contents = symbol;
    newLayer = fontTempLayer.duplicate();
    newLayer.rasterize (RasterizeType.TEXTCONTENTS);
    symbolBounds = newLayer.bounds;
    newLayer.remove();
    
    // The difference between the tallest character's starting Y position and the
    // comparing character's starting Y position is the vertical buffer.
    return symbolBounds[1] - tallestSymbolBounds[1];
}

// This does not necessarily mean the tallest symbol. We want to locate the character who's
// topmost pixels are the highest compared to every other character. This is typically a dollar
// sign or exclamation point - something that sticks up very high.
function determineHighestPositionedCharacter(symbolString)
{
    var tallestCharacter;
    var tallestCharacterRect;
    var rect;
    var c;
    
    for (var i = 0; i < symbolString.length; i++)
    {  
        c = symbolString.charAt(i);
        rect = calculateStringBoundsRect(c);
            
        if (tallestCharacterRect == null || rect.top < tallestCharacterRect.top)
        {
            tallestCharacter = c;
            tallestCharacterRect = rect;
        }
    }

    return tallestCharacter;
}

function determineSpaceCharacterWidth()
{
    var r1;
    var r2;
    
    // First, calculate the bounds of two letters side by side. I chose "AA" arbitrarily.
    // Next, calculate the width of the same two letters with a space between them.
    r1 = calculateStringBoundsRect("AA");
    r2 = calculateStringBoundsRect("A A");
    
    // Return the difference between the two widths
    return r2.width - r1.width;
}

function calculateStringBoundsRect(stringToCalculate)
{
    var result;
    var newLayer;
    
    // Set the contents of our text layer to the string passed in
    fontTempLayer.textItem.contents = stringToCalculate;
    
    // Duplicate the layer, and convert it into a standard art layer
    newLayer = fontTempLayer.duplicate();
    newLayer.rasterize (RasterizeType.TEXTCONTENTS);
    
    // Convert the new layer's bounds to a more usable rect object
    result = new rect(newLayer.bounds);
    
    // Delete the temporary layer we just created
    newLayer.remove();
    
    return result;
}

function fillNewCanvasArea(oldWidth, oldHeight)
{
    var width = doc.width;
    var height = doc.height;
    var selectionBounds;
    
    doc.activeLayer = backgroundLayer;
    
    if (width > oldWidth)
    {
        selectionBounds = [
            [oldWidth, 0], // top-left
            [width, 0], // top-right
            [width, height], // bottom-right
            [oldWidth, height]]; // bottom-left
            
        doc.selection.select(selectionBounds,SelectionType.REPLACE,0,false);
        doc.selection.fill(magentaColor);
    }

    if (height > oldHeight)
    {
        selectionBounds = [
            [0, oldHeight], // top-left
            [width, oldHeight], // top-right
            [width, height], // bottom-right
            [0, height]]; // bottom-left
            
        doc.selection.select(selectionBounds,SelectionType.REPLACE,0,false);
        doc.selection.fill(magentaColor);
    }
}

// Use with the new keyword to create a new rectangle instance.
// Example: var myRect = new rect(myLayer.bounds);
function rect(bounds)
{  
    this.left = bounds[0].value; // bounds[0] == top left corner x
    this.top = bounds[1].value; // bounds[1] == top left corner y
    this.right = bounds[2].value; // bounds[2] == bottom right corner x
    this.bottom = bounds[3].value; // bounds[2] == bottom right corner y
    
    this.width = this.right - this.left;
    this.height = this.bottom - this.top;
}

function trim(str) {
    str = str.replace(/^\s+|\s+$/g,"");
    str = str.replace(/(\r\n|\n|\r)/gm,"");
    return str;
}
