﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainWindowViewModel.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace FontBuilder.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows.Input;
    using System.Windows.Threading;

    /// <summary>
    /// Class definition for MainWindowViewModel.cs.
    /// </summary>
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        private string _saveDirectory;
        private string _fontSizesToBuild;
        private bool _buildCommonEnglishSymbols;
        private bool _buildExtendedASCIISymbols;
        private bool _buildKoreanHangul;
        private string _buildResult;
        private bool _overwrite;
        private bool _keepPhotoshopDocumentsOpen;
        private bool _hidePhotoshopWhileBuilding;
        private bool _keepPhotoshopOpen;
        private bool _useDelayForPhotoshopResponsiveness;
        private bool _createSpritefontFile;
        private string _status;
        private bool _isPaused;
        private int _secondsElapsed;
        private int _totalFontsCreated;
        private bool _isAborting;
        private bool _isClosing;
        private bool _isExecuting;
        private bool _hasPhotoshopBeenInitialized;
        private bool _isAdornerVisible;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindowViewModel"/> class.
        /// </summary>
        public MainWindowViewModel()
        {
            _keepPhotoshopOpen = true;
            _createSpritefontFile = true;
            _useDelayForPhotoshopResponsiveness = true;
            _buildCommonEnglishSymbols = true;
            _keepPhotoshopDocumentsOpen = true;
            _fontSizesToBuild = Globals.CommonFontSizes;
        }

        /// <summary>
        /// PropertyChanged event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets the UIDispatcher for this window. This will be used by our
        /// worker thread, as UI items can only be modified by the UIDispatcher thread.
        /// </summary>
        public Dispatcher UIDispatcher { get; set; }

        /// <summary>
        /// Gets or sets the directory in which we will be saving all generated 
        /// font files.
        /// </summary>
        public string SaveDirectory
        {
            get
            {
                return _saveDirectory;
            }

            set
            {
                _saveDirectory = value;
                OnPropertyChanged("SaveDirectory");
            }
        }

        /// <summary>
        /// Gets or sets a comma delimited string of the font sizes we want to build.
        /// </summary>
        public string FontSizesToBuild
        {
            get
            {
                return _fontSizesToBuild;
            }

            set
            {
                _fontSizesToBuild = value;
                OnPropertyChanged("FontSizesToBuild");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not we will be building a 
        /// font for the most common English symbols.
        /// </summary>
        public bool BuildCommonEnglishSymbols
        {
            get
            {
                return _buildCommonEnglishSymbols;
            }

            set
            {
                _buildCommonEnglishSymbols = value;
                OnPropertyChanged("BuildCommonEnglishSymbols");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not we will be building 
        /// a font for the extended ASCII symbols (includes all of the most 
        /// common English symbols).
        /// </summary>
        public bool BuildExtendedASCIISymbols
        {
            get
            {
                return _buildExtendedASCIISymbols;
            }

            set
            {
                _buildExtendedASCIISymbols = value;
                OnPropertyChanged("BuildExtendedASCIISymbols");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not we will be building a 
        /// font for Korean Hangul.
        /// </summary>
        public bool BuildKoreanHangul
        {
            get
            {
                return _buildKoreanHangul;
            }

            set
            {
                _buildKoreanHangul = value;
                OnPropertyChanged("BuildKoreanHangul");
            }
        }

        /// <summary>
        /// Gets or sets the build result. This is displayed to the user after 
        /// completion. Should be something along the lines of "Build Successful", 
        /// "Failed", or "Aborted".
        /// </summary>
        public string BuildResult
        {
            get
            {
                return _buildResult;
            }

            set
            {
                _buildResult = value;
                OnPropertyChanged("BuildResult");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not to overwrite and cleanup 
        /// old files in the save directory.
        /// </summary>
        public bool Overwrite
        {
            get
            {
                return _overwrite;
            }

            set
            {
                _overwrite = value;
                OnPropertyChanged("Overwrite");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not to keep all photoshop 
        /// documents open after the font building process has been completed.
        /// </summary>
        public bool KeepPhotoshopDocumentsOpen
        {
            get
            {
                return _keepPhotoshopDocumentsOpen;
            }

            set
            {
                _keepPhotoshopDocumentsOpen = value;
                OnPropertyChanged("KeepPhotoshopDocumentsOpen");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not to keep photoshop open 
        /// after the font building process has been completed.
        /// </summary>
        public bool KeepPhotoshopOpen
        {
            get
            {
                return _keepPhotoshopOpen;
            }

            set
            {
                _keepPhotoshopOpen = value;
                OnPropertyChanged("KeepPhotoshopOpen");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not to hide photoshop while 
        /// executing the font builder. This will hide it and remove it from the 
        /// task bar.
        /// </summary>
        public bool HidePhotoshopWhileBuilding
        {
            get
            {
                return _hidePhotoshopWhileBuilding;
            }

            set
            {
                _hidePhotoshopWhileBuilding = value;
                OnPropertyChanged("HidePhotoshopWhileBuilding");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not to use a small delay 
        /// between commands to prevent photoshop from choking and becoming 
        /// unresponsive.
        /// </summary>
        public bool UseDelayForPhotoshopResponsiveness
        {
            get
            {
                return _useDelayForPhotoshopResponsiveness;
            }

            set
            {
                _useDelayForPhotoshopResponsiveness = value;
                OnPropertyChanged("UseDelayForPhotoshopResponsiveness");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not to keep generate a 
        /// .spritefont file after building our font images. This .spritefont file 
        /// will be needed for XNA to understand the character ranges we will be 
        /// using. This is not required for "Common English Symbols".
        /// </summary>
        public bool CreateSpritefontFile
        {
            get
            {
                return _createSpritefontFile;
            }

            set
            {
                _createSpritefontFile = value;
                OnPropertyChanged("CreateSpritefontFile");
            }
        }

        /// <summary>
        /// Gets or sets the current status of the build process. This could be 
        /// a variety of conditions - PAUSED, Pausing..., Aborting..., etc).
        /// </summary>
        public string Status
        {
            get
            {
                return _status;
            }

            set
            {
                _status = value;
                OnPropertyChanged("Status");
            }
        }

        /// <summary>
        /// Gets the play/pause button text. When paused, the play/pause button 
        /// should display "Play". When executing, the play/pause button should 
        /// display "Pause".
        /// </summary>
        public string PlayPauseButtonText
        {
            get
            {
                if (IsPaused)
                    return "Resume";
                else
                    return "Pause";
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not the font build process 
        /// is currently paused.
        /// </summary>
        public bool IsPaused
        {
            get
            {
                return _isPaused;
            }

            set
            {
                _isPaused = value;

                OnPropertyChanged("IsPaused");
                OnPropertyChanged("PlayPauseButtonText");
            }
        }

        /// <summary>
        /// Gets or sets the total number of seconds elapsed during this build.
        /// </summary>
        public int SecondsElapsed
        {
            get
            {
                return _secondsElapsed;
            }

            set
            {
                _secondsElapsed = value;

                OnPropertyChanged("SecondsElapsed");
                OnPropertyChanged("TimerText");
                OnPropertyChanged("TimerTextLong");
            }
        }

        /// <summary>
        /// Gets or sets the total number of fonts created. This gets incremented
        /// after every individual font is completed. Even if a build is aborted, 
        /// this value could still be greater than zero.
        /// </summary>
        public int TotalFontsCreated
        {
            get
            {
                return _totalFontsCreated;
            }

            set
            {
                _totalFontsCreated = value;

                OnPropertyChanged("TotalFontsCreated");
                OnPropertyChanged("TotalFontsCreatedLabel");
            }
        }

        /// <summary>
        /// Gets a nicely formatted string for the user.
        /// </summary>
        public string TotalFontsCreatedLabel
        {
            get
            {
                return TotalFontsCreated + " font(s) created.";
            }
        }

        /// <summary>
        /// Gets the current time for the timer label. Converts the total number 
        /// of seconds elapsed into a nicely formatted timer string.
        /// </summary>
        public string TimerText
        {
            get
            {
                // Convert the seconds elapsed to a timespan for easy string formatting
                TimeSpan elapsed = new TimeSpan(_secondsElapsed * TimeSpan.TicksPerSecond);

                // Only display the hours if necessary. Always display the minutes and seconds
                if (elapsed.Hours > 0)
                    return string.Format(Globals.MoreThanOneHourDateFormat, elapsed);
                else
                    return string.Format(Globals.LessThanOneHourDateFormat, elapsed);
            }
        }

        /// <summary>
        /// Gets an even more verbose method of displaying the total seconds elapsed 
        /// to the user.
        /// </summary>
        public string TimerTextLong
        {
            get
            {
                return "Total Time Elapsed: " + TimerText;
            }
        }

        /// <summary>
        /// <para>
        /// Gets or sets a value indicating whether or not the user is aborting the
        /// build.
        /// </para>
        /// <para>
        /// Our worker thread will attempt to gracefully stop when the Abort button 
        /// is clicked. This means it will finish it's current task, and stop 
        /// immediately after. If it is in the process of executing an external 
        /// photoshop script, this could take several seconds.
        /// </para>
        /// <para>
        /// While we are in the process of aborting, all buttons
        /// need to be disabled.
        /// </para>
        /// </summary>
        public bool IsAborting
        {
            get
            {
                return _isAborting;
            }

            set
            {
                _isAborting = value;

                // Notify WPF that the property changed, so any GUI elements bound
                // to it will update themselves.
                OnPropertyChanged("IsAborting");

                // Tell all commands to recheck their canExecute methods. 
                RefreshCommands();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not the close/exit button 
        /// has been pressed. This is needed by the worker thread. It lets the 
        /// worker thread know that it shouldn't update any UI elements on the 
        /// UIDispatcher thread anymore. If we attempt to do something like set 
        /// our progress bars to invisible while the window is in a "closing" 
        /// situation, the application will hang.
        /// </summary>
        public bool IsClosing
        {
            get
            {
                return _isClosing;
            }

            set
            {
                _isClosing = value;
                OnPropertyChanged("IsClosing");
            }
        }

        /// <summary>
        /// <para>
        /// Gets or sets a value indicating whether or not our world builder is 
        /// executing.
        /// </para>
        /// <para>
        /// If the application is paused, IsExecuting will still return true. If the
        /// user clicks abort, and a task is still running for a few seconds afterwards,
        /// IsExecuting will still return true until the task is finished and the 
        /// thread is dead.
        /// </para>
        /// </summary>
        public bool IsExecuting
        {
            get
            {
                return _isExecuting;
            }

            set
            {
                _isExecuting = value;

                // Notify WPF that the property changed, so any GUI elements bound
                // to it will update themselves.
                OnPropertyChanged("IsExecuting");

                // Tell all commands to recheck their canExecute methods. 
                RefreshCommands();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not photoshop has been 
        /// initialized. This means that photoshop is open and the template document 
        /// has been created. We do not allow the user to pause the build until after 
        /// photoshop has at least been loaded, and the real build has been started.
        /// </summary>
        public bool HasPhotoshopBeenInitialized
        {
            get
            {
                return _hasPhotoshopBeenInitialized;
            }

            set
            {
                _hasPhotoshopBeenInitialized = value;

                // Notify WPF that the property changed, so any GUI elements bound
                // to it will update themselves.
                OnPropertyChanged("HasPhotoshopBeenInitialized");

                // Tell all commands to recheck their canExecute methods. 
                RefreshCommands();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not our ProgressOverlay is 
        /// currently visible.
        /// </summary>
        public bool IsAdornerVisible
        {
            get
            {
                return _isAdornerVisible;
            }

            set
            {
                _isAdornerVisible = value;
                OnPropertyChanged("IsAdornerVisible");
            }
        }

        /// <summary>
        /// Splits and parses our comma delimited string of sizes to build,
        /// and returns a list of integer sizes.
        /// </summary>
        /// <returns>An integer list of font sizes to build.</returns>
        public IEnumerable<int> GetSizesToBuildList()
        {
            List<int> sizes = new List<int>();

            // Split and parse our comma delimited string of sizes to build
            List<string> sizeStrings = FontSizesToBuild.Split(',').ToList();
            foreach (string s in sizeStrings)
            {
                sizes.Add(int.Parse(s));
            }

            return sizes;
        }

        /// <summary>
        /// Reset all variables to their default values. This will get
        /// called whenever the user clicks the rape button to start a
        /// new operation.
        /// </summary>
        public void Reset()
        {
            IsAborting = false;
            IsPaused = false;
            SecondsElapsed = 0;
            TotalFontsCreated = 0;
        }

        /// <summary>
        /// Create the OnPropertyChanged method to raise the event.
        /// </summary>
        /// <param name="name"></param>
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        /// <summary>
        /// Refreshes all commands.
        /// </summary>
        private void RefreshCommands()
        {
            // Make sure to call it on the UI thread, or it won't work.
            UIDispatcher.Invoke(
                DispatcherPriority.Normal,
                (Action)CommandManager.InvalidateRequerySuggested);
        }
    }
}
