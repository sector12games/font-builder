Tool for generating bitmap fonts for use in XNA projects.

Requires:
 - https://bitbucket.org/sector12games/sector12lib
 - Photoshop CC (2013) or higher

For more information, please see:
http://www.sector12games.com/font-builder/